-- This file only load on require

--
local M = {
	current_index = 0,
}

M.setup = function(opts)
	print("Options: ", opts)
end

local print_keys = function(maps)
	for _, value in ipairs(maps) do
		-- print(value)
		print("Keys: ", value.lhs, " Value: ", value.rhs)
	end
end


M.get_all_keys = function()
	local maps = vim.api.nvim_get_keymap("n")
	-- print(vim.inspect(maps))
	print_keys(maps)
end

M.create_new_buffer = function()
	local buf = vim.api.nvim_create_buf(true, false)
	if buf == 0 then
		print("Erro to create new buffer")
	end

	-- vim.api.nvim_buf_set_name(buf, "New note " .. tostring(M.current_index))
	-- M.current_index = M.current_index + 1
	vim.api.nvim_buf_set_text(buf, 0, 0, 0, 0, {"# ", "", "New Note"})
	vim.api.bo.filetype = "markdown"
	-- Open the new buffer in window
	vim.api.nvim_win_set_buf(0, buf)

	-- Create a window, like popup
	-- local win = vim.api.nvim_open_win(buf, true, {relative='win', row=3, col=3, width=12, height=6})
end

M.create_new_buffer()

-- M.get_all_keys()

return M
